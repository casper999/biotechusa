import Vue from 'vue'
import VueI18n from 'vue-i18n'
//
import router from './router'

import Product from './components/Application/Product.vue'
import Headers from './components/Header/Header.vue'

import Infomessage from './components/FeedBack/Feedback.vue'



import axios from 'axios'




var store = {
    language: {
        lang: "HU",
    },
    editor: {
        content: ""
    },
    tags: {
        selected: []
    },
    product: {
        data: {},
        image: "",
        price: "",
        currencyID: "",
        publish: ""
    },
    resetStore: function () {
        this.product.image = ""
        this.product.price = ""
        this.product.currencyID = ""
        this.product.publish = ""
        this.tags.selected = []
        this.product.data = {}
        this.language.lang = "HU"
        this.editor.content = ""
    },
    setPriceAction(value) {
        this.product.price = value
    },
    setCurrencyIDAction(value) {
        this.product.currencyID = value
    },
    setPublishAction(value) {
        this.product.publish = value
    },



    setLangAction(value) {
        this.language.lang = value
    },
    setEditorAction(value) {
        this.editor.content = value
    },
    setSelectedAction(value) {
        this.tags.selected = value
    },
    setDataAction(value) {
        this.product.name = value
    },
    setImageAction(value) {
        this.product.image = value
    }
};

var editor = {
    content: "",
    setContentAction(value) {
        this.content = value
    },
    resetState() {
        this.content = ""
    }
}


var tags = {
    selected: {},
    setSelectedAction(value) {
        this.selected = value
    }
}

var languages = {
    selected: '',

    data: [],
    setDataAction(value) {
        this.data = value
    },
    setSelectedAction(value) {
        this.selected = value
    }
}

var product = {
    data: {},
    productID: undefined,
    image: "",
    price: "",
    currencyID: "",
    publish: [],
    //
    setDataAction(value) {
        this.data = data
    },
    setImageAction(value) {
        this.image = value
    },
    setPriceAction(value) {
        this.price = value
    },
    setCurrencyIDAction(value) {
        this.currencyID = value
    },
    setPublishAction(value) {
        this.publishBegin = value
    },
    setProductIDAction(value) {
        this.productID = value
    },

    resetState() {
        this.productID = undefined,
        this.image = ""
        this.price = ""
        this.currencyID = ""
        this.publish = []

    }

}

var products = {
    data: [],
    setDataAction(value) {
        this.data = value
    }
}

var detailData = {
    productID: '',
    setProductAction(value) {
        this.product = value
    }
}

var infos = {
    msg: [],
    setMsgAction(value) {
        this.msg = value
    },


};

var validationError = {
    errors: {
        image: "",
        price: "",
        currencyID: "",
        publish: "",
    },
    setErrorsAction(value) {
        this.errors = value
    },
    setImageAction(value) {
        this.errors.image = value
    },
    setPriceAction(value) {
        this.errors.price = value
    },
    setCurrencyIDAction(value) {
        this.errors.currencyID = value
    },
    setPublishAction(value) {
        this.error.publish = value
    }
}

const app = new Vue({
    el: "#app",
    router,
    components: {
        Product,
        Headers,
        Infomessage
    },
    data: {
        title: "Biotech usa",
        store: store,
        infos: infos,
        products: products,
        languages: languages,
        editor: editor,
        product: product,
        detail: detailData,
        tags: tags,
        error: validationError,
        detailLoaded: true
    },
    created() {
        this.getLanguages()
    },
    mounted() {

    },
    methods: {
        resetState() {
            this.initedData()
            this.product.resetState()
            this.editor.resetState()
            this.$emit("RESET-STATE")
        },
        getLanguages() {
            var that = this
            axios.get('/langs').then(response => {
                that.languages.data = response.data
                that.languages.selected = response.data[0].langCode
                that.initedData()
                that.$emit("INITED-DATA")
            }).catch(error => {
                console.log(error)
            })
        },
        initedData() {
            this.languages.data.forEach(item => {
                let data = {
                    name: "",
                    description: ""
                }

                this.product.data[item.langCode] = data
                this.tags.selected[item.langCode] = []
            });
        },
        validateData() {
            this.validateNameAndDesc()
            this.validatePublish()
            this.validatePrice()
            this.validateCurrency()
            // 
            this.validateImage()
            //
            return this.setInfoMessage()
            
        },
        setInfoMessage() {
            var msgs = []

            for (var key in this.error.errors) {
                //
                let item = this.error.errors[key]
                if (Array.isArray(item)) {
                    for (var i = 0; i < item.length; i++) {
                        let msg = item[i]
                        msgs.push(msg)
                    }
                } else if (item != "") {
                    msgs.push(item)
                }
            }
            if (msgs.length > 0) {
                this.infos.msg = msgs
                return false
            }
            return true
        },
        validatePrice() {
            var pattern = /(\d+(\.\d+)?)/
            var check = pattern.test(this.product.price)
            if (check) {
                this.error.errors.price = ""
                return
            }
            this.error.errors.price = "Adja meg az árat!"
        },
        validatePublish() {
            if (this.product.publish.length > 0) {
                this.error.errors.publish = ""
                return
            }
            this.error.errors.publish = "Adja meg a publikálás időintervallumát!"
        },
        validateImage() {
            if (this.product.image == "" || this.product.image == "assets/upload_image.svg") {
                this.error.errors.image = "Töltsön fel képet!"
            } else {
                this.error.errors.image = ""
            }
        },
        validateCurrency() {
            if (this.product.currencyID == "") {
                this.error.errors.currencyID = "Válassza ki a pénznemet!"
                return
            }
            this.error.errors.currencyID = ""
        },
        validateNameAndDesc() {
            var msg = []
            var desc = []
            for (var key in this.product.data) {
                let item = this.product.data[key]
                if (item.name == "") {
                    msg.push(this.getLanguageName(key) + ": Hiányzó név")

                }
                if (item.description == "") {
                    desc.push(this.getLanguageName(key) + ": Hiányzó leírás")
                }
            }
            this.error.errors.name = msg
            this.error.errors.desc = desc
        },
        showErrors($error) {
            this.error.errors = $error.error
            this.setInfoMessage()

        },
        getLanguageName(langCode) {
            for (var key in this.languages.data) {
                let lang = this.languages.data[key]
                if (lang.langCode == langCode) {
                    return lang.name
                }
            }
        },
        closeError() {
            this.infos.msg = []
        },
        showDetail(id) {
            this.detail.productID = id
            router.push({ path: 'product/detail' })
        },
        closeDetail() {
            router.go(-1)
        },
        editProduct(item) {
            let url = '/productdata/' + item.id
            var that = this
            router.push({ path: '/product/add' })
            axios.get(url).then(response => {
                that.languages.selected = "HU"
                that.product.productID = item.id
                that.product.data = response.data.data
                that.product.price = item.price
                that.product.currencyID = item.currencyID
                that.product.image = "/storage/" + item.imageName
                that.tags.selected = response.data.tags
                that.product.publish = []
                that.$emit("LOADED-EDIT-DATA", item)
                //
                let bDate = item.publishBegin.replace(" ", "T")
                let eDate = item.publishEnd.replace(" ", "T")
                that.product.publish.push(new Date(bDate))
                that.product.publish.push(new Date(eDate))

            }).catch(error => {
                console.log(error)
            })
        }
    }

})