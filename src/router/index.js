import Vue from 'vue'
import Router from 'vue-router'
// import home from '@/components/home'
import Product from '../components/Application/Product.vue'
import Products from '../components/Application/Products.vue'
import Detail from '../components/Application/ProductDetails.vue'

Vue.use(Router)
let router = new Router({
  routes: [
    {
      path: '/',
      name: 'products',
      component: Products
    },
    {
      path: '/product/add',
      name: 'product',
      component: Product
    },
    {
      path: '/product/detail',
      name: 'deatil',
      component: Detail
    },
  ]
})

router.beforeEach((to, from, next) => {
    //Todo authentication
    console.log("Todo Auth here")
    next()
})

export default router