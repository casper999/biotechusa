# Biotechusa Test case

### Technology
- PHP 7.3
- Lumen
- Vue JS
- Tailwind css
- Docker



### Docker
- install Docker desktop
- https://www.docker.com/products/docker-desktop
- run command in your app folder
```bash
$ docker-compose up
```

### Database
Connect docker db
- Host: localhost
- Port: 8003 (docker)
- user: biotechusa
- pass: avrq666

Mysql Workbench:
https://www.mysql.com/products/workbench/

Navicat
https://www.navicat.com/en/

Run two sql files 
- create_db_structure.sql
- insert_default_data.sql

These file find in Database directory.

Database model in Database directory.
- biotechusa_db_model.pdf
- biotechusa_db_model.ndm (navicat model file)

### Install lumen
```bash
$ cd application
$ composer install
```

### See app
localhost:8000














