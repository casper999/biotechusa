/*
 Navicat Premium Data Transfer

 Source Server         : Biotechusa
 Source Server Type    : MariaDB
 Source Server Version : 100404
 Source Host           : localhost:8003
 Source Schema         : biotechusa

 Target Server Type    : MariaDB
 Target Server Version : 100404
 File Encoding         : 65001

 Date: 28/10/2019 00:54:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for currencies
-- ----------------------------
DROP TABLE IF EXISTS `currencies`;
CREATE TABLE `currencies` (
  `id` char(60) COLLATE utf8_hungarian_ci NOT NULL COMMENT 'currency code',
  `name` char(60) COLLATE utf8_hungarian_ci DEFAULT NULL COMMENT 'currancy name',
  `code` char(4) COLLATE utf8_hungarian_ci DEFAULT NULL COMMENT 'currency code',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `idx_curr_name` (`name`),
  KEY `idx_curr_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci COMMENT='Table store currency data';

-- ----------------------------
-- Table structure for langs
-- ----------------------------
DROP TABLE IF EXISTS `langs`;
CREATE TABLE `langs` (
  `langCode` char(4) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `name` char(60) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `imageID` char(60) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  UNIQUE KEY `idx_langs_lang_code` (`langCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci COMMENT='Table store lang data';

-- ----------------------------
-- Table structure for productImages
-- ----------------------------
DROP TABLE IF EXISTS `productImages`;
CREATE TABLE `productImages` (
  `id` char(60) COLLATE utf8_hungarian_ci NOT NULL COMMENT 'image identifier',
  `productID` char(60) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `name` varchar(320) COLLATE utf8_hungarian_ci DEFAULT NULL COMMENT 'path to image',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_images_path` (`name`),
  KEY `fk_image_product_id` (`productID`),
  CONSTRAINT `fk_image_product_id` FOREIGN KEY (`productID`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci COMMENT='Table store images path';

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` char(60) COLLATE utf8_hungarian_ci NOT NULL COMMENT 'product identifier',
  `name` char(60) COLLATE utf8_hungarian_ci DEFAULT NULL COMMENT 'product name text code',
  `desc` char(60) COLLATE utf8_hungarian_ci DEFAULT NULL COMMENT 'product descritpion text code',
  `publishBegin` datetime DEFAULT NULL COMMENT 'product publish begin',
  `publishEnd` datetime DEFAULT NULL COMMENT 'product publish end',
  `price` decimal(10,4) DEFAULT NULL COMMENT 'product price',
  `currencyID` char(60) COLLATE utf8_hungarian_ci DEFAULT NULL COMMENT 'product price currency id (HUF, USD, etc ... )',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_products_name` (`name`),
  KEY `idx_products_desc` (`desc`),
  KEY `idx_products_publish_begin` (`publishBegin`),
  KEY `idx_products_publish_end` (`publishEnd`),
  KEY `idx_products_currency` (`currencyID`),
  KEY `idx_products_price` (`price`),
  CONSTRAINT `fk_products_currency_id` FOREIGN KEY (`currencyID`) REFERENCES `currencies` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_products_desc_text_code` FOREIGN KEY (`desc`) REFERENCES `texts` (`textCode`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_products_name_text_code` FOREIGN KEY (`name`) REFERENCES `texts` (`textCode`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci COMMENT='Table store product data';

-- ----------------------------
-- Table structure for products_tags_map
-- ----------------------------
DROP TABLE IF EXISTS `products_tags_map`;
CREATE TABLE `products_tags_map` (
  `productID` char(60) COLLATE utf8_hungarian_ci NOT NULL,
  `tagID` char(60) COLLATE utf8_hungarian_ci DEFAULT NULL,
  KEY `fk_products_tags_map_product_id` (`productID`),
  KEY `fk_products_tags_map_tag_id` (`tagID`),
  CONSTRAINT `fk_products_tags_map_product_id` FOREIGN KEY (`productID`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_products_tags_map_tag_id` FOREIGN KEY (`tagID`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci COMMENT='Table connect product and tags';

-- ----------------------------
-- Table structure for tags
-- ----------------------------
DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
  `id` char(60) COLLATE utf8_hungarian_ci NOT NULL,
  `name` char(60) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_tags_name` (`name`),
  CONSTRAINT `fk_tags_name_text_code` FOREIGN KEY (`name`) REFERENCES `texts` (`textCode`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci COMMENT='Table store tag data';

-- ----------------------------
-- Table structure for texts
-- ----------------------------
DROP TABLE IF EXISTS `texts`;
CREATE TABLE `texts` (
  `textCode` char(60) COLLATE utf8_hungarian_ci NOT NULL,
  `langCode` char(4) COLLATE utf8_hungarian_ci NOT NULL,
  `text` mediumtext COLLATE utf8_hungarian_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  UNIQUE KEY `unq_idx_lang_text_code` (`textCode`,`langCode`),
  KEY `idx_texts_code` (`textCode`),
  KEY `idx_texts_text` (`text`(1024)),
  KEY `idx_texts_lang_code` (`langCode`),
  CONSTRAINT `fk_texts_langCode` FOREIGN KEY (`langCode`) REFERENCES `langs` (`langCode`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci COMMENT='Table store all text for all languages';

-- ----------------------------
-- View structure for productsView
-- ----------------------------
DROP VIEW IF EXISTS `productsView`;
CREATE ALGORITHM=UNDEFINED DEFINER=`biotechusa`@`%` SQL SECURITY DEFINER VIEW `productsView` AS (select `t1`.`id` AS `id`,`t1`.`name` AS `nameCode`,`t4`.`text` AS `name`,`t1`.`desc` AS `desc`,`t1`.`price` AS `price`,`t1`.`publishBegin` AS `publishBegin`,`t1`.`publishEnd` AS `publishEnd`,`t1`.`currencyID` AS `currencyID`,`t2`.`name` AS `imageName`,`t3`.`code` AS `code` from (((`products` `t1` join `texts` `t4` on(`t1`.`name` = `t4`.`textCode` and `t4`.`langCode` = 'HU')) left join `productImages` `t2` on(`t1`.`id` = `t2`.`productID`)) left join `currencies` `t3` on(`t1`.`currencyID` = `t3`.`id`)));

SET FOREIGN_KEY_CHECKS = 1;
