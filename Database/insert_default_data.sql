INSERT INTO currencies (id, name, code, updated_at, created_at)
VALUES 
(UUID(), 'Forint', 'HUF', NOW(), NOW()),
(UUID(), 'Usa dollár', 'USD', NOW(), NOW()),
(UUID(), 'Euro', 'EUR', NOW(), NOW());


INSERT INTO langs
(langCode, name) VALUES
('HU', 'Magyar'),
('EN', 'English'),
('DE', 'Deutsch');