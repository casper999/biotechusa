<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

use Illuminate\Support\Facades\DB;

class ProductTags extends Model
{
    protected $table = "products_tags_map";

    protected $fillable = [
        "productID",
        "tagID"
    ];

    protected $keyType = 'string';
    public $incrementing = false;
    public $timestamps = false;

}
