<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;

class Text extends Model
{


    protected $table = "texts";

    protected $fillable = [
        "textCode",
        "langCode",
        "text"
    ];

    protected $keyType = 'string';
    public $incrementing = false;


}