<?php

namespace App\Interfaces;

interface LangInterface {
    public function createLang($langCode, $name);
}
