<?php

namespace App\Interfaces;

interface ProductOutputInterface {
    public function getProducts();
    public function getProduct($id, $langCode);
    public function getProductTags($id, $langCode);
    public function getProductTextData($id, $langCode);
    public function getProductAllTags($id);
}
