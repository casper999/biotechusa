<?php

namespace App\Interfaces;

interface TagInterface {
    public function createTag($tag, $lang);
}