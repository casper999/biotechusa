<?php

namespace App\Interfaces;

interface TextInterface {
    public function createText($text, $lang);
    public function createTextWithCode($textCode, $lang, $text);
    public function getText($textCode, $lang);
    public function deleteText($textCode, $lang);
}
