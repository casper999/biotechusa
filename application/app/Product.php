<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

use Illuminate\Support\Facades\DB;

class Product extends Model
{
    protected $table = "products";

    protected $fillable = [
        "id",
        "name",
        "desc",
        "imageID",
        "publishBegin",
        "publishEnd",
        "price",
        "currencyID"
    ];

    protected $keyType = 'string';
    public $incrementing = false;


}
