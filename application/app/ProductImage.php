<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;

class ProductImage extends Model
{


    protected $table = "productImages";

    protected $fillable = [
        "id",
        "productID",
        "name"
    ];

    protected $keyType = 'string';
    public $incrementing = false;


}
