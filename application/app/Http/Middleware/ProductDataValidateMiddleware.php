<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Currency;
use App\Lang;
class ProductDataValidateMiddleware
{
    private $data;
    private $error = [];
    public function handle(Request $request, Closure $next)
    {
        $this->data = $request->all();
        $this->validateData();
        if (!empty($this->error)) {
            $response = [
                "error" => $this->error
            ];
            return response($response, 400);
        }
        return $next($request);


    }
    //
    private function validateData() {
        // return false;
        $this->validatePrice();
        $this->validateImage();
        $this->validatePublish();
        $this->validateCurrency();
        $this->validateNameAndDesc();
    }
    //
    private function validatePrice() {

        $pattern = "/(\d+(\.\d+)?)/";
        $price = $this->data["product"]["price"];
        if(preg_match($pattern, $price)){
            return;
        }
        $this->error["price"] = "Adja meg az árat!";

    }
    private function validateImage() {
        $image = $this->data["product"]["image"];
        if ( preg_match('/^\/storage/', $image) ) {
            return;
        }
        if (!preg_match('/^data:image\/(\w+);base64,/', $image)) {
            $this->error["image"] = "Töltsön fel képet!";

        }


    }
    private function validatePublish() {

        $publish = $this->data["product"]["publish"];
        if ( is_array($publish) ) {
            if ( count($publish) > 0 ) {
                return;
            }
        }
        $this->error["publish"] = "Adja meg a publikálás időintervallumát!";
    }
    private function validateCurrency() {
        $currencyID = $this->data["product"]["currencyID"];
        $currency = Currency::where('id', $currencyID)->get()->first();
        if (!empty($currency)) {
            return;
        }
        $this->error["currency"] = "Nincs ilyen pénznem";


    }
    private function validateNameAndDesc() {
        // $nameErrors = [];
        $nameErrors = [];
        $descErrors = [];
        $missingLang = [];
        $data = $this->data["product"]["data"];
        foreach ($data as $key => $val) {
            $lang = Lang::where("langCode", $key)->get()->first();

            if (empty($lang)) {
                array_push($missingLang, "Nincs ilyen nyelv ($key)");
            } else {
                if (empty($val["name"])) {
                    array_push($nameErrors, "Hiányzó név ($key)");
                }
                if (empty($val["description"])) {
                    array_push($descErrors, "Hiányzó leírás ($key)");
                }
            }
        }
        //
        if (!empty($nameErrors)) {
            $this->error["name"] = $nameErrors;
        }
        if (!empty($descErrors)) {
            $this->error["desc"] = $descErrors;
        }
        if (!empty($missingLang)) {
            $this->error["lang"] = $missingLang;
        }
    }
}
