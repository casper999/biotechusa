<?php

namespace App\Http\Controllers;

use Validator;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Interfaces\ProductInterface;
use App\Interfaces\ProductOutputInterface;


class ProductController extends BaseController
{
    //
    private $repository;
    private $outputRepository;
    public function __construct(ProductInterface $repository, ProductOutputInterface $outputRepository) {
        $this->repository = $repository;
        $this->outputRepository = $outputRepository;
    }


    public function index() {
        return view('biotechusa');
    }

    public function save(Request $request) {
        $data = $request->all();
        $response = $this->repository->save($data);
        return response()->json($response);
    }

    public function getProducts() {
        $response = $this->outputRepository->getProducts();
        return response()->json($response);
    }

    public function getProduct($id, $langCode) {
        $response = $this->outputRepository->getProduct($id, $langCode);
        return response()->json($response);
    }
    public function getProductTextData($id, $langCode) {
        $response = $this->outputRepository->getProductTextData($id, $langCode);
        return response()->json($response);
    }

    public function getProductTags($id, $langCode) {
        $response = $this->outputRepository->getProductTags($id, $langCode);
        return response()->json($response);
    }
    public function getAllTags($id) {
        $response = $this->outputRepository->getProductAllTags($id);
        return response()->json($response);
    }

    public function getProductData($id) {
        $response = $this->outputRepository->getProductData($id);
        return response()->json($response);
    }




}
