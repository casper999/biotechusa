<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Interfaces\TagInterface;
use App\Tag;



class TagController extends BaseController
{

    private $repository;
    public function __construct( TagInterface $repository ) {
        $this->repository = $repository;
    }
    //
    public function getTags() {
        // $model = new Tag();
        // $result = $model->getTags();
        // return response()->json($result);
    }
    //
    public function getTagsByLang($lang) {
        $result = $this->repository->getTagsByLang($lang);
        return response()->json($result);
    }
    //
    public function addTag(Request $request) {
        $data = $request->all();
        $tag = $data["tag"];
        $lang = $data["lang"];
        $result = $this->repository->createTag($tag, $lang);
        $response = [
            "name" => $tag,
            "code" => $result->id
        ];
        return response()->json($response);
    }

}
