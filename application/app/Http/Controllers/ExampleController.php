<?php

namespace App\Http\Controllers;



class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index() {
        // $date = new \DateTime('2019-10-17T22:00:00.000Z');
        // echo $date;
        $time = strtotime('2019-10-17T22:00:00.000Z');

        $newformat = date('Y-m-d H:i:s',$time);

        echo $newformat;
    }
    //
}
