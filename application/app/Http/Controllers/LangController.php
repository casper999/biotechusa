<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

use \App\Lang;

class LangController extends BaseController
{
    //
    public function getLangs() {
        $model = new Lang();
        $result = $model->getLangs();
        return response()->json($result);
    }
}
