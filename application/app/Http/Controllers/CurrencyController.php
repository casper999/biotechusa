<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Interfaces\CurrInterface;
use App\Tag;



class CurrencyController extends BaseController
{
    private $repository;
    public function __construct( CurrInterface $repository ) {
        $this->repository = $repository;
    }
    //
    public function getCurrencies() {
        $response = $this->repository->getCurrencies();
        return response()->json($response);

    }

}
