<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;


class StorageController extends BaseController
{
    //
    public function index($filename) {
        $headers = ["Content-Type" => "image/png"];
        if ($filename == "undefined" || $filename == "null") {
            $content = \Storage::get('placeholder.svg');
            $headers = ["Content-Type" => "image/svg+xml"];
        } else {
            $content = \Storage::get($filename);
        }
        return response($content)->withHeaders($headers);
    }





}
