<?php

namespace App\Repositories;

use App\Interfaces\TagInterface;
use App\Repositories\TextRepository;

use App\Tag;
use Illuminate\Support\Facades\DB;


class TagRepository extends BaseRepository implements TagInterface {


    private $textRepository;

    public function createTag($tag, $lang) {
        $this->textRepository = new TextRepository();
        $text = $this->textRepository->createText($tag, $lang);
        //
        $uuid = $this->generateUUID();
        return Tag::create([
            "id" => $uuid,
            "name" => $text->textCode
        ]);

    }
    //
    public static function getTagsByLang($lang) {
        $results = DB::select("SELECT t2.text as name, t1.id as code FROM tags t1
        INNER JOIN texts t2 ON t1.name = t2.textCode AND t2.langCode = ?", [$lang]);
        return $results;
    }
}
