<?php
namespace App\Repositories;

use App\Interfaces\ProductOutputInterface;

use Illuminate\Support\Facades\DB;
use App\Product;
use App\Lang;
class ProductOutputRepository extends BaseRepository implements ProductOutputInterface {

    public function getProducts() {
        $sql = "SELECT
                    t1.id,
                    t1.price,
                    t1.publishBegin,
                    t1.publishEnd,
                    t1.currencyID,
                    t3.name as imageName,
                    t4.code
                FROM
                    products t1
                INNER JOIN productImages t3 on t1.id = t3.productID
                INNER JOIN currencies t4 on t1.currencyID = t4.id";
        $results = \DB::select($sql);
        return $results;
    }


    public function getProductTextData($id, $langCode) {
        $sql = "SELECT
                t3.text as name,
                t4.text as productDesc
            FROM
                products t1
            INNER JOIN texts t3 ON t1.name = t3.textCode AND t3.langCode = ?
            INNER JOIN texts t4 ON t1.desc = t4.textCode AND t4.langCode = ?
            WHERE t1.id = ?
            ";
         $results = \DB::select($sql, array($langCode, $langCode, $id));
         if (!empty($results)) {
            $result = $results[0];
            return $result;
         }
         return [];
    }

    public function getProduct($id, $langCode) {
        $product = [];
        $sql = "SELECT
                    t1.id,
                    t1.name as nameCode,
                    t4.text as name,
                    t5.text as productDesc,
                    t1.price,
                    t1.publishBegin,
                    t1.publishEnd,
                    t1.currencyID,
                    t2.name as imageName,
                    t3.code
                FROM
                    products t1
                INNER JOIN
                    texts t4 on t1.name = t4.textCode AND t4.langCode = ?
                INNER JOIN
                    texts t5 on t1.desc = t5.textCode AND t5.langCode = ?
                LEFT JOIN
                    productImages t2 on t1.id = t2.productID
                LEFT JOIN
                    currencies t3 on t1.currencyID = t3.id
                WHERE t1.id = ?";
        $products = \DB::select($sql, array($langCode, $langCode, $id));
        if (!empty($products)) {
            $product = $products[0];
        }
        $response = [
            "product" => $product
        ];
        return $response;
    }

    public function getProductTags($id, $langCode) {
        $sql = "SELECT
                    t1.id,
                    t1.name as textCode,
                    t2.text as name
                FROM
                    tags t1
                INNER JOIN products_tags_map t3 on t1.id = t3.tagID AND t3.productID = ?
                INNER JOIN texts t2 on t1.name = t2.textCode AND t2.langCode = ?";
        $results = \DB::select($sql, array($id, $langCode));
        return $results;
    }
    public function getProductAllTags($id) {
        $sql = "SELECT
                    t1.id as code,
                    t1.name as textCode,
                    t2.text as name,
                    t2.langCode
                FROM
                    tags t1
                INNER JOIN products_tags_map t3 on t1.id = t3.tagID AND t3.productID = ?
                INNER JOIN texts t2 on t1.name = t2.textCode";
        $results = \DB::select($sql, array($id));

        $tags = [];
        // for ($i = 0; $i < count($results); $i++) {
        //     $item = $results[i];
        //     // dd($item);

        // }
        if ( count($results) > 0 ) {
            foreach ($results as $values) {
                $langCode = $values->langCode;
                if ( !array_key_exists($langCode, $tags) ) {
                    $tags[$langCode] = [];
                    array_push($tags[$langCode], $values);
                } else {
                    array_push($tags[$langCode], $values);
                }

            }
        } else {
            $model = new Lang();
            $langs = $model->getLangs();
            foreach ($langs as $lang) {
                $code = $lang->langCode;
                $tags[$code] = [];
            }
        }


        return $tags;
    }
    public function getProductData($id) {
        $tags = $this->getProductAllTags($id);
        $data = [];
        $sql = "SELECT
                t4.text AS NAME,
                t4.langCode
            FROM
                products t1
                INNER JOIN texts t4 ON t1.NAME = t4.textCode
            WHERE
                t1.id = ?";
        $results = \DB::select($sql, array($id));
        //
        foreach ($results as $value) {
            $langCode = $value->langCode;
            $data[$langCode] = [];
            $data[$langCode]["name"] = $value->NAME;
        }
        //
        $sql = "SELECT
                t4.text AS description,
                t4.langCode
            FROM
                products t1
                INNER JOIN texts t4 ON t1.desc = t4.textCode
            WHERE
                t1.id = ?";
        $results = \DB::select($sql, array($id));
        foreach ($results as $value) {
            $langCode = $value->langCode;
            $data[$langCode]["description"] = $value->description;
        }
        //
        $response = [
            "data" => $data,
            "tags" => $tags
        ];
        return $response;
    }

}
