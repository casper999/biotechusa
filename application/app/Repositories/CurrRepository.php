<?php

namespace App\Repositories;

use App\Interfaces\CurrInterface;

use App\Currency;
use Illuminate\Support\Facades\DB;

class CurrRepository implements CurrInterface {

    public function getCurrencies() {
        return Currency::all();
    }

}
