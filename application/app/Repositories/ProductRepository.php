<?php

namespace App\Repositories;

use App\Interfaces\ProductInterface;
use App\Product;
use App\ProductImage;
use App\ProductTags;
use App\Repositories\TextRepository;
use App\Text;
use Illuminate\Support\Facades\DB;

// use Illuminate\Support\Facades\Storage;

class ProductRepository extends BaseRepository implements ProductInterface
{

    private $id;

    private $data;
    private $tags;
    private $image;

    private $product;

    private $textRepository;

    public function __construct()
    {
        $this->textRepository = new TextRepository();
    }

    public function save($data)
    {
        $this->data = $data["product"];
        $this->tags = $data["tags"]["selected"];
        $this->image = $this->data["image"];
        //
        if (!empty($this->data["productID"])) {
            $this->id = $this->data["productID"];
            $this->product = Product::where("id", $this->id)->get()->first();
            return $this->updateProduct();
        }
        return $this->createProduct();
        // return $this->tags;
    }
    public function getProducts()
    {

    }
    public function getProduct($id)
    {

    }
    //
    private function createProduct()
    {
        $this->id = $this->generateUUID();
        //
        $publishBegin = $this->getPublishDate($this->data["publish"][0]);
        $publishEnd = $this->getPublishDate($this->data["publish"][1]);

        $product = Product::create([
            "id" => $this->id,
            "price" => $this->data["price"],
            "currencyID" => $this->data["currencyID"],
            "publishBegin" => $publishBegin,
            "publishEnd" => $publishEnd,
        ]);
        $this->saveNameAndDescription();
        $this->addTags();
        $this->saveImage();
        return $product;
    }
    private function updateProduct()
    {
        //
        $publishBegin = $this->getPublishDate($this->data["publish"][0]);
        $publishEnd = $this->getPublishDate($this->data["publish"][1]);
        //
        $product = Product::where("id", $this->id)->update(
            [
                "price" => $this->data["price"],
                "currencyID" => $this->data["currencyID"],
                "publishBegin" => $publishBegin,
                "publishEnd" => $publishEnd,
            ]);
        $this->updateNameAndDescription();
        $diff = $this->updateTags();
        // $product = Product::where("id", $this->id)->get()->first();
        return $diff;
    }
    //
    private function updateNameAndDescription()
    {
        $nameTextcode = $this->product["name"];
        $descTextcode = $this->product["desc"];
        //
        $data = $this->data["data"];
        foreach ($data as $key => $val) {
            Text::where("textCode", $nameTextcode)->where("langCode", $key)->update(["text" => $val["name"]]);
            Text::where("textCode", $descTextcode)->where("langCode", $key)->update(["text" => $val["name"]]);
        }
    }

    //
    private function saveNameAndDescription()
    {
        $data = $this->data["data"];

        $nameTextCode = $this->generateUUID();
        $descTextCode = $this->generateUUID();

        foreach ($data as $key => $val) {

            if (!empty($val["name"])) {
                $this->saveTextCode($nameTextCode, $val["name"], $key, "name");
            }
            if (!empty($val["description"])) {
                $this->saveTextCode($descTextCode, $val["description"], $key, "desc");
            }
        }
    }
    private function saveTextCode($textCode, $text, $langCode, $column)
    {
        $this->textRepository->createTextWithCode($textCode, $langCode, $text);
        Product::where("id", $this->id)->update([$column => $textCode]);
    }
    //
    private function getPublishDate($dateString)
    {
        $time = strtotime($dateString);
        return date('Y-m-d H:i:s', $time);
    }
    //
    private function updateTags()
    {
        $sql = "DELETE t2 FROM
                        tags t1
                    INNER JOIN products_tags_map t2
                    ON t1.id = t2.tagID WHERE t2.productID = ?";
        $tags = \DB::select($sql, array($this->id));
        //Add tags
        try {
            $this->addTags();
        } catch(Exception $exception) {

        }

        // return $diff;

    }

    private function addTags()
    {
        foreach ($this->tags as $tags) {
            foreach ($tags as $tag) {
                //Insert ignore
                try {
                    // ProductTags::create([
                    //     "productID" => $this->id,
                    //     "tagID" => $tag["code"],
                    // ]);
                    $sql = "INSERT IGNORE products_tags_map (tagID, productID) VALUES(?, ?)";
                    \DB::select($sql, array($tag["code"], $this->id));

                } catch (Exception $exception) {
                    //this is not error
                }

            }

        }
    }
    //
    private function updateImage()
    {
        if (preg_match('/^data:image\/(\w+);base64,/', $this->image)) {
            $imageName = $this->id . ".png";
            \Storage::delete($imageName);
            //
            $data = substr($this->image, strpos($this->image, ',') + 1);
            $data = base64_decode($data);
            //
            \Storage::disk('local')->put($imageName, $data);

        }
    }

    private function saveImage()
    {
        if (preg_match('/^data:image\/(\w+);base64,/', $this->image)) {
            $data = substr($this->image, strpos($this->image, ',') + 1);

            $data = base64_decode($data);
            $imageName = $this->id . ".png";
            \Storage::disk('local')->put($imageName, $data);
            //
            $uuid = $this->generateUUID();
            ProductImage::create(
                [
                    "id" => $uuid,
                    "productID" => $this->id,
                    "name" => $imageName,
                ]
            );

        }

    }

}
