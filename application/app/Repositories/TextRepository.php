<?php

namespace App\Repositories;

use App\Interfaces\TextInterface;
use App\Text;

class TextRepository extends BaseRepository implements TextInterface {

    public function createText($text, $lang) {
        $uuid = $this->generateUUID();
        return $this->createTextWithCode($uuid, $lang, $text);

    }
    public function createTextWithCode($textCode, $lang, $text) {
        return Text::create([
            "textCode" => $textCode,
            "langCode" => $lang,
            "text" => $text
        ]);
    }

    public function getText($textCode, $lang) {
    }
    public function deleteText($textCode, $lang) {

    }
    //



}
