<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

use Illuminate\Support\Facades\DB;

class Tag extends Model
{


    protected $table = "tags";

    protected $fillable = [
        "id",
        "name",
    ];

    protected $keyType = 'string';
    public $incrementing = false;
   

}
