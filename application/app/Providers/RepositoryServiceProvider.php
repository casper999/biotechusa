<?php
namespace App\Providers;

use App\Repositories\TagRepository;
use App\Interfaces\TagInterface;
//
use App\Inerfaces\TextInterface;
use App\Repositories\TextRepository;

use App\Interfaces\ProductInterface;
use App\Repositories\ProductRepository;

use App\Interfaces\CurrInterface;
use App\Repositories\CurrRepository;

use App\Interfaces\ProductOutputInterface;
use App\Repositories\ProductOutputRepository;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bind the interface to an implementation repository class
     */
    public function register()
    {
        $this->app->bind(
            TagInterface::class,
            TagRepository::class,
        );
        $this->app->bind(
            TextInterface::class,
            TextRepository::class,
        );
        $this->app->bind(
            ProductInterface::class,
            ProductRepository::class
        );
        $this->app->bind(
            CurrInterface::class,
            CurrRepository::class
        );
        $this->app->bind(
            ProductOutputInterface::class,
            ProductOutputRepository::class
        );
    }
}
