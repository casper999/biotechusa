<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// $router->get('/', function () use ($router) {
//     return $router->app->version();
// });
$router->get('/', function ()  {
    return view('biotechusa');
});


$router->group(['middleware' => 'request'], function () use ($router) {
    $router->get('/tags', 'TagController@getTags');
    $router->get('/tags/{langCode}', 'TagController@getTagsByLang');
    $router->get('/products', 'ProductController@getProducts');
    $router->get('/product/{id}/{langCode}', 'ProductController@getProduct');
    $router->get('/product/data/{id}/{langCode}', 'ProductController@getProductTextData');
    $router->get('/product/tags/{id}/{langCode}', 'ProductController@getProductTags');
    $router->get('/tags/product/{id}', 'ProductController@getAllTags');
    $router->get('/productdata/{id}', 'ProductController@getProductData');


    $router->get('/langs', 'LangController@getLangs');
    $router->get('/currencies', 'CurrencyController@getCurrencies');
    $router->post('/tag/add', 'TagController@addTag');

});

$router->group(['middleware' => 'request', 'middleware' => 'validation'], function () use ($router) {
    $router->post('/product/save', 'ProductController@save');


});

$router->get('storage/{filename}', 'StorageController@index');






// $router->get('/products', 'TagController@getTags');


// $router->get('/product/new', 'ProductController@index');

