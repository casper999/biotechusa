<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Biotechusa Product managment</title>
  <meta name="description" content="Biotechusa">
  <meta name="author" content="SitePoint">
  <link rel="stylesheet" href="css/app.css?v=<?php echo time();?>">

</head>

<body>

  <div id="app">
    <Infomessage></Infomessage>
    <Headers></Headers>
    <router-view/>
  </div>
  <script src="js/app.js?v=<?php echo time();?>"></script>
</body>
</html>
